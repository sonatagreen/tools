#!/bin/bash

declare _callname
if [ "$(basename $0)" = "moon" ]; then _callname=moon; else _callname="$0"; fi
declare _usage="\
Usage: $_callname <command> [<latency>]

Commands:
  \033[1mhelp\033[0m:  show this help text.
  \033[1mstate\033[0m: detect and show current system state (vanilla/on/off).
  \033[1mstart\033[0m: set state to on, first doing \033[1msetup\033[0m if necessary.
  \033[1mstop\033[0m:  set state from on to off.
  \033[1msetup\033[0m: set state from vanilla to off. Hopefully you should never
         have to do this yourself.

<latency> requires a unit, e.g. 100ms or 60s
 If not supplied, the LATENCY environment variable is used if present,
 or otherwise 1260ms (the lightspeed delay between Earth and the Moon).
 Note that the latency applies to both incoming and outgoing traffic,
 so the round-trip delay is twice the given value.

This script was put together essentially as a convenience shorthand for a 
 series of magic invocations I barely understand. I advise against relying
 on an assumption that it reflects either expertise or even competence.
 You might want to read the source before attempting to use it. Good luck."




# Before anything else, do a couple of basic sanity checks.

# First, do we have too many arguments?
if [ -n "$3" ]; then
    echo -e "$_usage" >&2
    exit 1
fi

# Second, does the system pattern-match to one of the known sane states?
# This may or may not work as intended on anyone else's computer beside mine.
# We do use _sysstate for further checks later on, though, so if you set
# CHECKSTATE=0 then you'll have to run `moon setup` yourself. Sorry.
declare _checkstate
if [ -n "$CHECKSTATE" ]; then
    _checkstate=$(( 0 + $CHECKSTATE ))
else
    #declare _checkstate=0
    declare _checkstate=1
fi

declare _sysstate=unset
if [ "$_checkstate" -gt 0 ]; then
    declare _tcql="$(tc qdisc list)"
    case "$_tcql" in
        # qdisc pfifo_fast 0: dev eth0 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
        # qdisc mq 0: dev wlan0 root
        *'qdisc mq 0: dev wlan0 root ')
            _sysstate=vanilla
            ;;
        
        # qdisc pfifo_fast 0: dev eth0 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
        # qdisc netem 8003: dev wlan0 root refcnt 5 limit 1000 delay 1.3s
        # qdisc ingress ffff: dev wlan0 parent ffff:fff1 ----------------
        # qdisc netem 8002: dev ifb0 root refcnt 2 limit 1000 delay 1.3s"
        *delay*delay*)
            _sysstate=on
            ;;
        
        # qdisc pfifo_fast 0: dev eth0 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
        # qdisc mq 0: dev wlan0 root 
        # qdisc ingress ffff: dev wlan0 parent ffff:fff1 ---------------- 
        # qdisc pfifo_fast 0: dev ifb0 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1
        *priomap*priomap*)
            _sysstate=off
            ;;
        
        *)
            echo "Unrecognized system state:" >&2
            tc qdisc list >&2
            echo "
(This is probably my fault, not yours. You might try running the script with 
CHECKSTATE=0. If you get into a totally borked state and can't figure out how 
to fix it, it should always be possible to escape by restarting your computer; 
changes shouldn't persist across a reboot. Good luck.)" >&2
            exit 2
            ;;
    esac
fi



# How far shall we be from the rest of the world?
# Note the round-trip delay is twice this.
declare _latency
if [ -n "$2" ]; then           # Prefer the command line value...
     _latency="$2"
elif [ -n "$LATENCY" ]; then   # ...then the environment variable...
    _latency="$LATENCY"
else                           # ...and last of all we fall back on the default.
    _latency=moon
fi

# Recognize some keyword latencies.
case "$_latency" in
    moon|luna)
        _latency=1260ms   # Earth-Moon distance: 1.26 light-seconds.
        ;;
    sun|sol|au)
        _latency=499s     # Earth-Sun distance (1 AU): surprisingly close to 499 light-seconds (8m19s).
        ;;
esac



# Syntactic sugar: strip leading '-'s,
# so e.g. 'help' '-help' '--help' are all equivalent.
declare _cmd=$1
while [ -n "$(echo "$_cmd")" ] && [ $(echo $_cmd | head -c 1) = '-' ]; do
    _cmd=$(echo $_cmd | tail -c +2)
done

# Process command.
case "$_cmd" in
    setup)
        if [ "$_checkstate" -eq 0 -o $_sysstate = "vanilla" ]; then
            sudo modprobe ifb
            sudo ip link set dev ifb0 up
            sudo tc qdisc add dev wlan0 ingress
            # TODO: figure out how to undo this last line,
            #       so we can have a teardown command.
            sudo tc filter add dev wlan0 parent ffff: \
                 protocol ip u32 match u32 0 0 \
                 flowid 1:1 action mirred egress \
                 redirect dev ifb0
        else
            echo "Error: refusing to run 'setup' from non-vanilla state" >&2
            exit 3
        fi
        ;;
    
    start)
        if [ "$_checkstate" -gt 0 -a $_sysstate = "vanilla" ]; then
            $0 setup
        fi && # this && *is* doing something, unlike its counterpart further down
            sudo tc qdisc replace dev wlan0 root netem delay "$_latency" && #out
            sudo tc qdisc replace dev  ifb0 root netem delay "$_latency"    #in
        ;;
    
    stop)
        if [ "$_checkstate" -gt 0 -a $_sysstate = "vanilla" ]; then
            echo "Error: refusing to run 'off' from vanilla state" >&2
            exit 4
        fi && # this && is unnecessary but i like how it makes the lines line up
            sudo tc qdisc delete dev wlan0 root &&
            sudo tc qdisc delete dev  ifb0 root
        ;;

    state)
        echo $_sysstate
        ;;
    
    h|help|'')
        echo -e "$_usage"
        ;;
    
    *)
        echo -e "$_usage" >&2
        exit 1
        ;;
esac
